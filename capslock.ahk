;=============================================================================================
; Show a ToolTip that shows the current state of the lock keys (e.g. CapsLock) when one is pressed
;=============================================================================================
~*NumLock::
~*CapsLock::
~*ScrollLock::
    Sleep, 10	                                        ; drastically improves reliability on slower systems (took a loooong time to figure this out)

    msg := "Caps Lock: " (GetKeyState("CapsLock", "T") ? "ON" : "OFF") "`n"
    msg := msg "Num Lock: " (GetKeyState("NumLock", "T") ? "ON" : "OFF") "`n"
    msg := msg "Scroll Lock: " (GetKeyState("ScrollLock", "T") ? "ON" : "OFF")

    ToolTip, %msg%
    Sleep, 1000	                                        ; SPECIFY DISPLAY TIME (ms)
    ToolTip		                                        ; remove
    return
;=============================================================================================
; Show a ToolTip that shows the current state of the lock keys (e.g. CapsLock) when one is pressed
;=============================================================================================