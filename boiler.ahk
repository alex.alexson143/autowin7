#SingleInstance Force
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetTitleMatchMode, Regex
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

;-------------------------; HOTKEYS ;-------------------------;
^!+Esc::ExitApp  ; Fail safe procedure
  
  ;-------------------------; Reload at save ;-------------------------; a
  
  #IfWinActive, AES_25Jun16.ahk .* SciTE4AutoHotkey
    ~^s::
      Traytip, Reloading updated script, %A_ScriptName%, ,
      SetTimer, RemoveTrayTip, 2000
      Sleep, 2000
      Reload
    return
  #IfWinActive
  
  RemoveTrayTip:
    SetTimer, RemoveTrayTip, Off 
    TrayTip 
  return 